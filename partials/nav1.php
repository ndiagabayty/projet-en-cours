<nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php"><?= WEBSITE_NAME?></a>
        </div>
        
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="list_user.php">Liste des utilisateurs</a></li>
            <li>
                <input type="search" name="" placeholder="Rechercher un utilisateur" id="searchbox" class="form-control">&nbsp;<i class="fa fa-spinner fa-spin loader" id="loader"></i>
                <div id="display-result">
                    
                    
                    
                </div>
            </li>
          </ul>
          <ul class="nav navbar-nav  navbar-right">
            <li class="<?= set_active('index') ?>"><a href="index.php">Accueil</a></li>

            <?php if (is_logged_in()): ?>
             
             <!---  Affichage des notifications !-->
            <li class="<?= $notifications_count > 0 ? 'have_notifs' : '' ?>">
               <a href="notifications.php"><i class="fa fa-bell"></i>
               <?= $notifications_count > 0 ? "($notifications_count)" : ''; ?>
               </a>
            </li>

            <?php else: ?>  
            <li class="<?= set_active('login') ?> "><a href="login.php">Connexion</a></li>
            <li class="<?= set_active('register') ?> "><a href="register.php">Inscription</a></li>
          <?php endif; ?>
          </ul>
        </div><!--/.nav-collapse -->
      </div> 
    </nav>


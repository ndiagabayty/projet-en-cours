<link rel="stylesheet" type="text/css" href="assets/css/micropost.css"/>
   <article class="media status-media" id="micropost<?=$micropost->m_id ?>"  >
     <div class="pull-left">
         <img src="<?= $micropost->avatar ? $micropost->avatar: get_avatar_url($micropost->email) ?>"
          alt="<?= $micropost->pseudo ?>" class="media-object avatar-xs img-circle"
           >
         
     </div>
     <div class="media-body"  >
         <h4 class="media-heading"><?= echappe($micropost->pseudo); ?></h4>
         <img src="image/png/alarm-clock.png " style="width: 27px;">  <span class="timeago" title="<?= $micropost->created_at ?>">
         <?= $micropost->created_at ?></span>

         <?php if($micropost->id_user == get_session('id_user')): ?>
              <a   data-confirm = "Voulez vous vraiment Supprimer cette publication?"
                 href="delete_micropost.php?id=<?= $micropost->m_id ?>">
                 <img src="image/png/garbage-2.png " style="width: 25px;">
                 Supprimer
              </a>
         <?php endif; ?> 
         
         </p>
         <?= nl2br(replace_links(echappe($micropost->content))); ?>
          <p>
              <?php if(user_has_already_like_the_micropost($micropost->m_id)): ?>
                  <a id="unlike<?=$micropost->m_id ?>" data-action="unlike"  class="like" 
                   href="unlike_micropost.php?id=<?=$micropost->m_id?>"> 
                   Je n'aime plus
                   </a>
              <?php else : ?>
                  <a id="like<?=$micropost->m_id ?>" data-action="like" class="like" 
                   href="like_micropost.php?id=<?=$micropost->m_id?>"><i class="jaime" ></i>J'aime
                  </a>
              <?php endif ;?>

            </p>

              <div id="likers_<?=$micropost->m_id ?>">

                <?= get_likers_text($micropost->m_id) ?>

              </div>
        
     </div>
   </article>

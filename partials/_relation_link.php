  <?php if(relation_link_to_display($_GET['id'])==CANNEL_RELATION):?>

    <p>Demande d'amitie deja envoye.</br>
    <a class="btn btn-primary pull-right" href="delete_friends.php?id=<?= $_GET['id'] ?>">
     <i class="fa fa-ban"></i> Annuler la demande</a>
    </p>
  
  <?php elseif(relation_link_to_display($_GET['id'])==ACCEPET_REJET_RELATION): ?>
      <a class="btn btn-success " href="accept_friends.php?id=<?= $_GET['id'] ?>"><i class="fa fa-check"></i> Accepter</a>
      <a class="btn btn-danger " href="delete_friends.php?id=<?= $_GET['id'] ?>"><i class="fa fa-ban"></i> Decliner</a>

  <?php elseif(relation_link_to_display($_GET['id'])==DELETE_RELATION): ?>
   <a class="btn btn-primary pull-right" href="delete_friends.php?id=<?= $_GET['id'] ?>"><i class="fa fa-ban"></i> Retirer de ma liste d'amis</a>
   
  <?php elseif(relation_link_to_display($_GET['id'])==ADD_RELATION): ?>

     <a href="add_friends.php?id=<?= $_GET['id'] ?>" 
     class="btn btn-primary pull-right">
     <i class="fa fa-plus"></i> Ajouter comme ami</a>
  <?php endif;?>

.<?php
 session_start();
 require("includes/init.php");
 require("filters/auth_filter.php");
 

 if (!empty($_GET['id']) && $_GET['id']==get_session('id_user')) {
  	 // Recuperer les infos de l'utilisateur depuis la base de donnees
  	 $user = find_user_by_id($_GET['id']);

  	  if (!$user) {
  	  	  redirection('index.php');
  	  }
  }else{
  	redirection('profile.php?id='.get_session('id_user'));
  }
   // le formulaire a ete soumis
   if (isset($_POST['update'])) {

   	    $erros =[];

     	 // si tous les champs ont ete remplies
     	if (no_empty(['name','city','country','sexe','bio']) )
        {
           extract($_POST);

        $query= $db->prepare("UPDATE users SET name =:name,city =:city,
        	country=:country,sexe =:sexe,twitter =:twitter,github =:github,
        	avaible_for_hiring =:avaible_for_hiring, bio =:bio WHERE id =:id");

     		$query->execute(
              [
                'name'=>$name,
                'city'=>$city,
                'country'=>$country,
                'sexe'=>$sexe,
                'twitter'=>$twitter,
                'github'=>$github,
                'avaible_for_hiring'=>!empty($avaible_for_hiring) ? '1' : '0',
                'bio'=>$bio,
                'id'=>get_session('id_user')
              ]);

               set_flash("Felicitation, votre profile a ete mis a jour!");
               redirection('profile.php?id='.get_session('id_user'));
        }else{
            // On garde les donnees saisies dans champs inputs
            garder_infos_saisis();
            $erros[] = "Veuillez remplir tous les champs marques d'un (*)" ;
        }

      
    } else{
     // permettant de nettoyer les donnees garder en session
     supprimer_les_donnees();
   }
   require("views/edit_user.view.php"); 
 
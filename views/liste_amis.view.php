
<?php $tilte='Liste des utilisateurs';?>


<?php include("partials/_header.php"); ?>

   <link rel="stylesheet" type="text/css" href="assets/css/liste-ami.css">

    
    <div class="main-content">

       <div class="container">
            <div class="panel panel-primary" id="panel-pos">
           <div class="panel-heading">
              <h3 class="panel-title"><i class=" fa fa-users"></i> Liste de mes  amis  </h3>
           </div>
           <div class="panel-body" >
          <?php foreach(array_chunk($users, 1) as $user_set): ?>
             <ul class="list-group " >
               <?php foreach($user_set as $user): ?>
                  <li class="list-group-item " >
                        <a href="profile.php?id=<?=echappe($user->id) ?>">
                      <img src="<?= $user->avatar ? $user->avatar :
                              get_avatar_url($user->email,30) ?>" width ="50" height="50"
                              alt ="<?=echappe($user->pseudo) ?>"
                              class= "img-circle">
                        </a>
                
                  <h4 class="user-block-username">
                     <a href="profile.php?id=<?=echappe($user->id) ?>">
                         <?=echappe($user->pseudo) ?>
                     </a>

                      <a class="btn btn-primary" 
                             href="new_message.php?id=<?=$user->id?>">
                          <i class="fa fa-envelope " style="color:#4f4;"></i>
                           Message</a>
                    
                  </h4>
            </li>
          <?php endforeach?>
          </ul>          
          <?php endforeach?>
         </div>  

        </div>
        <div id="pagination"><?=$pagination ?></div>
      </div>
    </div>
    

     <?php include('partials/_footer.php'); ?>

 
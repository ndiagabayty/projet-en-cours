
<?php $tilte='Inscription';?>


<?php include("partials/_header.php"); ?>

  <link rel="stylesheet" type="text/css" href="../assets/css/register.css"/>
    
  <body>
  <!-- <p id="icone"><img src="image/register1.png"></p> -->
    <!-- <img class="pane-titlet" src="image/register1.png"> -->
    <div class="main-content" >
         <div class="container" id="locat" >
        <div class="col-md-6 col-md-offset-3" >
            <div class="panel panel-primary panel-position" id="formulaire" >
            <div class="panel-heading" id="entete">
               <!--  <h3 class="panel-title">Devenez deja present membre!</h3> -->
               <p id="icone"><img src="image/register1.png"></p>
            </div>
            <div class="panel-body">
          

          <?php
                    include("partials/_error.php");
          ?>

          <form data-parsley-validate  method="post"   >

            <!--  name field  -->
            <div class="form-group">
              <label class="control-label" for="name"><img src="image/png/edit-validated-icon.png" style="height:40px;width:40px;"> Nom:</label>
              <input class="form-control" value="<?= recupere_infos_saisis('name') ?>" type="text" name="name" id="name" required="required">
            </div>

             <!-- pseudo field  -->
            <div class="form-group">
              <label class="control-label" for="pseudo"><img src="image/png/pseudo.png" style="height:40px;width:40px;">Pseudo:</label>
              <input class="form-control" value="<?= recupere_infos_saisis('pseudo') ?>" type="text" name="pseudo" id="pseudo" required="required" data-parsley-minlength="3">
            </div>

             <!-- email field -->
            <div class="form-group">
              <label class="control-label" for="email"><img src="image/png/email.png" style="height:30px;width:35px;"> Adresse Email:</label>
              <input class="form-control" value="<?= recupere_infos_saisis('email') ?>" type="email" name="email" id="email" required="required" data-parsley-trigger="keypress">
            </div>

              <!-- password field  -->
            <div class="form-group">
              <label class="control-label" for="password"><img src="image/png/locked.png" style="height:30px;width:35px;">Mot de Passe:</label>
              <input class="form-control" type="password" name="password" id="password" required="required">
            </div>

             <!-- Fconfirmation field  -->
            <div class="form-group">
              <label class="control-label" for="password_confirm"><img src="image/png/locked.png" style="height:30px;width:35px;">Confirmer votre mot de passe:</label>
              <input class="form-control" type="password" name="password_confirm" id="password_confirm" required="required" data-parsley-equalto="#password">
            </div>
            
            <input class="btn btn-primary " type="submit" name="register" value="Inscription">

          </form>

         </div>  

         </div>
      </div>  

    </div>
    </div>

     </body>

     <?php include("partials/_footer.php"); ?>



<?php $tilte='Liste des utilisateurs';?>


<?php include("partials/_header.php"); //<h1 id="list-user">Liste des utilisateurs</h1> ?>
    


<link rel="stylesheet" type="text/css" href="assets/css/list-user.css"/>


    
    <div class="main-content">        
        <div class="container">
          <div class="panel panel-primary" id="panel-pos">
             <div class="panel-heading">
                <h3 class="panel-title"><i class=" fa fa-users"></i> Listes des utilisateurs  </h3>
             </div>
             <div class="panel-body" >
                 
                 <?php foreach(array_chunk($users, 1) as $user_set): ?>
                  <ul class="list-group " >
                  
                   <?php foreach($user_set as $user): ?>
                    <li class="list-group-item " >
                          <a href="profile.php?id=<?=echappe($user->id) ?>">
                            <img src="<?= $user->avatar ? $user->avatar :
                              get_avatar_url($user->email,30) ?>" width ="30" height="30"
                              alt ="<?=echappe($user->pseudo) ?>"
                              class= "img-circle">
                          </a>
                           <a href="profile.php?id=<?=echappe($user->id) ?>">
                             <?=echappe($user->pseudo) ?>
                           </a>
                           <!-- Si les deux utilisateur sont amis  !-->
                          <?php if(current_user_is_freind_with($user->id)) : ?>
                          <a class="btn btn-primary " 
                             href="new_message.php?id=<?=$user->id?>">
                          <i class="fa fa-envelope " style="color:#4f4;"></i>
                          Message</a>
                          <?php else: ?>
                          <a class="btn btn-primary "
                             href="add_friends.php?id=<?=$user->id?>"> 
                             
                          <i class="fa fa-plus " style="color:#4f4;"></i>
                          Ajouter</a>
                          <?php endif; ?>
                      </li>
                    

                    <?php endforeach?>
                  
                  </ul>
                  <?php endforeach?>
                 
             </div>
         </div>
          
        </div>
       

          <div id="pagination"><?=$pagination ?></div>
           

    </div>
    

     <?php include('partials/_footer.php'); ?>

 
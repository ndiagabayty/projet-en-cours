
<?php $tilte='Page de profile';?>

<!-- <link rel="stylesheet" type="text/css" href="assets/css/menu.css"/> -->
<link rel="stylesheet" type="text/css" href="assets/css/profile.css"/>
<?php include("partials/_header.php"); ?>


   
   
 
 <body> 
    <div class="main-content">
         
        <div class="container" id="content-primary">
            <div class="row" >
            	<div class="col-md-6"  >

            	    <div class="panel panel-primary">
		                <div class="panel-heading">
		                	<h3 class="panel-title">Profil de <?=echappe($_SESSION['pseudo']) ?>
                      (<?= friends_count($_GET['id'])?> ami<?= friends_count($_GET['id'])==1 ?'' :'s' ?> )</h3>
		                </div>
		                <div class="panel-body">
                          
                        <div class="row" >
                           <div class="col-md-5">
                           	 <img src="<?= $user->avatar ? $user->avatar:
                              get_avatar_url($user->email,100) ?>" width ="70" height="70"
                           	 	alt ="image de profil de <?=echappe($user->pseudo) ?>"
                               class= "img-circle">
                           </div>

                            <div col-md-7>
                            <?php if(!empty($_GET['id']) && $_GET['id']!==get_session('id_user')):?>
                              <?php require("partials/_relation_link.php") ; ?>
                            <?php endif;?>  
                           </div>
                           </div>

                           <div class="row">
                           	 <div class="col-sm-6">
                           	 <strong><?= echappe($user->pseudo); ?></strong></br>
                           	 <a href="mailto:<?= echappe($user->email)?>"> 
                           	  <?= echappe($user->email) ;?> </a></br>
                           	   <?=
                           	      $user->city && $user->country ?
                           	      '<i class="fa fa-location-arrow"></i>&nbsp;'
                           	      .echappe($user->city).'-'.echappe($user->country).'</br>'
                           	      : '' ;
                           	    ?><a href="https://www.google.com/maps?q=<?=echappe($user->city).' '.echappe($user->country) ?> " target= "_blank">Voir sur google maps</a>
                           	  
                           	 	
                           	 </div>
                           	 <div class="col-sm-6">
                           	    <?=
                           	        $user->twitter ? 
                           	        '<i class="fa fa-twitter"></i>&nbsp; 
                           	         <a href="//twitter.com/'.echappe($user->twitter).'">@'.echappe($user->twitter).'</a></br>' : '';
                           	    ?>
                           	    <?=
                           	        $user->github ? 
                           	        '<i class="fa fa-github"></i>&nbsp;
                           	         <a href="//github.com/'.echappe($user->github).'">'.echappe($user->github).'</a></br>' : '';
                           	    ?>
                           	    <?=
                           	       $user->sexe == 'H' ? 
                           	       '<i class="fa fa-male"></i>'
                           	       : '<i class="fa fa-female"></i>';

                           	    ?>
                           	    <?=
                           	       $user->avaible_for_hiring ? 
                           	        'disponible pour emploi'
                           	       :'Non disponible pour emploi';

                           	    ?>
                           	 	
                           	 </div>
                           </div>
                           <div class="row">
                           	<div class="col-md-12 well">
                           		<h5>Petite biographie de <?= echappe($user->name);?></h5>
                           		<p>
                           			<?=
                           			  $user->bio ? nl2br(echappe($user->bio)) : 'Aucune biographie pour le moment' ;
                           			?>

                           		</p>
                           	</div>
                           </div>
		                </div>
                    </div>   
            		
            	</div>

            	<div class="col-sm-6">
                <?php if(!empty($_GET['id']) && $_GET['id']==get_session('id_user')): ?>
                  <div class="status-post"> 
                  <div class="panel panel-primary" >
                    <div class="panel-heading" id="publication">
                       <h2 class="panel-title">Faites une publication</h2>
                    </div>
                  </div>
                   <form  action="microposts.php" method="post" data-parsley-validate >
                      <div class="form-group">
                        <label class="sr-only" for="content">Statut:</label>
                        <textarea class="form-control" name="content" id="content"
                         rows="3" required="required" maxlength="140" minlength="3" placeholder="Alors quoi de neuf ?" >  
                         </textarea>
                      </div>
                      <div class="form-group status-post-submit">
                        <input type="submit" name="publier" value="Publier" class="btn btn-success">
                      </div>
                   </form>
                  </div> 
                <?php endif; ?>


                <?php if(current_user_is_freind_with($_GET['id'])): ?>
                  <?php if (count($microposts)!=0) :?> 
                  <div class="panel panel-primary" id="title-pub" >
                    <div class="panel-heading" id="publication">
                       <h2 class="panel-title">Les Publications Recentes</h2>
                    </div>
                  </div>
                <div class="content-secondary">
                     <?php foreach ($microposts as $micropost) :?>
                       <?php include('partials/_micropost.php');?>
                     <?php endforeach; ?>
                    <?php else: ?>
                 <!-- <p>Cet utilisateur n'a encore rien poste pour le moment...</p>       -->
                  <?php endif; ?> 
                <?php endif; ?> 
              </div>

            </div>

          
          <?php
                    include("partials/_error.php");
          ?>

         </div>  

    </div>
    
</body>  
    
 
    <script src="assets/js/jquery.min.js"></script>
    <script  src="librairies/sweetalert/sweetalert.min.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="librairies/uploadify/jquery.uploadify.min.js"></script>
    <script src="librairies/parsley/parsley.min.js"></script>
    <script src="librairies/parsley/i18n/fr.js"></script>
    <script src="assets/js/jquery.timeago.js"></script>
    <script src="assets/js/jquery.timeago.fr.js"></script>
    
    
    <script type="text/javascript">
      window.ParsleyValidator.setLocale('fr')
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
        $(".timeago").timeago();

        $('.like').on('click',function(e){
          e.preventDefault();

          // var href = $(this).attr("href");
          // var parts = href.split("=");
          // var id = parts[1];
          // var part = href.split('_');
          // var action = part[0];

          // alert(action);
          var url = "ajax/like_micropost.php";
          var id = $(this).attr('id');
          var action = $(this).data('action');
          var micropost_id = id.split("like")[1];
          //console.log(action,micropost_id);
          //var data = "micropost_id= "+micropost_id+"&action="+action;

          $.ajax({
            type: 'POST',
            url:  url,
            data : {
              micropost_id: micropost_id,
              action: action


            },
            success: function(likers){
              
                
                $("#likers_"+micropost_id).html(likers);
                if (action =='like') {
                    $('#'+id).html("Je n'aime plus").data('action','unlike');
                    //$('.jaime').html("fa fa-thumbs-up");
                }else{
                    $('#'+id).html("J'aime").data('action','like');
                }

            }

          });

        });
      });
        window.ParsleyValidator.setLocale('fr') 
    </script>
     <?php include('partials/_footer.php'); ?>
<?php  
 require("bootstrap/locale.php");
 require("includes/functions.php");
 require("includes/constants.php");
 require("config/database.php"); 

  if (!empty($_COOKIE['pseudo']) && !empty($_COOKIE['id_user']) ) {
  	 
  	 $_SESSION['pseudo'] = $_COOKIE['pseudo'];
  	 $_SESSION['id_user'] = $_COOKIE['id_user'];
  	 $_SESSION['avatar'] = $_COOKIE['avatar'];
  }

  //Récupération du nombre total de notifications non lues
 $q = $db->prepare("SELECT id FROM notifications
 WHERE id_subject = ? AND seen = '0'");

 $q->execute([get_session('id_user')]);

 $notifications_count = $q->rowCount();

 //Récupération du nombre total de messages non lues
 $q = $db->prepare("SELECT id FROM message
 WHERE id_content = ? AND seen = '0'");

 $q->execute([get_session('id_user')]);
 
 $message_count = $q->rowCount();


 auto_login();

 ?>
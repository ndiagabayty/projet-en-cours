<?php
 session_start();
 require("includes/init.php");
 include('filters/auth_filter.php');

 // Nous récupérons toutes les notifications de l'utilisateur connecté
 // (Aussi bien les notifications lues que les notifications non lues).
 $q = $db->prepare( "SELECT users.pseudo, users.avatar, users.email,
					 notifications.id_subject,
					 notifications.name, notifications.id_user,
					 notifications.seen, notifications.created_at
					 FROM notifications
					 LEFT JOIN users ON users.id = id_user
					 WHERE id_subject = ?
					 ORDER BY notifications.created_at DESC
					 ");

 $q->execute([get_session('id_user')]);

 // Nous les stockons au niveau de la variable $notifications
 $notifications = $q->fetchAll(PDO::FETCH_OBJ);

 // Après avoir récupéré les notifications de l'utilisateur connecté,
 // nous modifions la valeur de leur attribut 'seen' afin d'indiquer que
 // l'utilisateur vient de lire ces notifications.
 $q = $db->prepare("UPDATE notifications SET seen = '1' WHERE id_subject = ?");
 $q->execute([get_session('id_user')]);
// Views
 <?php $title = "Notifications"; ?>
<?php include('partials/_header.php'); ?>

 <div id="main-content">
 <div class="container">
 <h1 class="lead">Vos notifications</h1>

 <?php if(count($notifications) > 0): ?>
 <ul class="list-group">

 <?php foreach($notifications as $notification): ?>
 <li class="list-group-item
 <?= $notification->seen == '0' ? 'not_seen' : '' ?>"
 >
 <?php require("partials/notifications/{$notification->name}.php"); ?>
 </li>
 <?php endforeach; ?>
 </ul>
 <?php else: ?>
 <p>Aucune notification disponible pour l'instant.</p>
 <?php endif; ?>
 </div>
 </div>
 
 <?php include('partials/_footer.php'); ?>

 <?php // Nous affichons ensuite le contenu de notre fichier notifications.view.php ?>
 <?php require("views/notifications.view.php"); ?>